//
//  PopularMoviesViewController.swift
//  TheMovieDB
//
//  Created by vortizes on 2/1/19.
//  Copyright © 2019 vortizes. All rights reserved.
//

import UIKit
import SDWebImage

class PopularMoviesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, InfoButtonDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    // initial page
    var currentPage : Int = 1
    
    // our custom object with the collection of movies
    var movieCollection : MovieCollection?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // configuring TableView
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieTableViewCell")
        
        // load popular movies for the first time
        popularMoviesByPage(page: currentPage)
    }
    
    func popularMoviesByPage(page: Int) -> Void {
        
        APIManager.sharedInstance.retrieveMoviesByPage(page: page, filter: "/popular", onSuccess: { (result) in
            
            self.movieCollection = result
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.configureNavBar()
            }
            
        }, onFailure: { error in
            
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            self.show(alert, sender: nil)
        })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.movieCollection?.movies?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MovieTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath as IndexPath) as! MovieTableViewCell
        cell.delegate = self
        cell.indexPath = indexPath
        
        let row = indexPath.row
        
        let movie = self.movieCollection?.movies?[row]
        
        if movie?.posterPath != nil {
            
            let imageUrl = APIManager.sharedInstance.posterImageEndpoint + (movie?.posterPath)!
            cell.movieImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
        }
        
        cell.movieLabel?.text = movie?.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }

    func infoButtonTapped(at index: IndexPath) {
        
        let vc = BottomViewController()
        let movie = self.movieCollection?.movies![index.row]

        vc.configureView(movie: movie!)
        vc.modalPresentationStyle = .custom
        present(vc, animated: true, completion: nil)
    }
 
    @objc func previousPage() -> Void {
        
        self.currentPage -= 1
        popularMoviesByPage(page: self.currentPage)
    }
    @objc func nextPage() -> Void {
        
        self.currentPage += 1
        popularMoviesByPage(page: self.currentPage)
    }
    
    func configureNavBar() -> Void {
        
        // customizing Navigation bar
        self.navigationItem.title = "Popular Movies"
        
        let previousPage = UIBarButtonItem.init(title: "Prev Page", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.previousPage))
        let nextPage = UIBarButtonItem.init(title: "Next Page", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.nextPage))
        
        if self.currentPage > 1 {
            self.navigationItem.leftBarButtonItem = previousPage
        } else {
            self.navigationItem.leftBarButtonItem = nil
        }
        if self.currentPage < (self.movieCollection?.totalPages)! {
            self.navigationItem.rightBarButtonItem = nextPage
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
    }
}

