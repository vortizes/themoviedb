//
//  TheMovieDBTests.swift
//  TheMovieDBTests
//
//  Created by vortizes on 2/1/19.
//  Copyright © 2019 vortizes. All rights reserved.
//

import XCTest
@testable import TheMovieDB

class TheMovieDBTests: XCTestCase {

    var jsonString : String?
    var movieCollection : MovieCollection?
    var movie : Movie?
    
    override func setUp() {
        
        jsonString = "{\"page\":1,\"total_results\":19809,\"total_pages\":991,\"results\":[{\"vote_count\":935,\"id\":450465,\"video\":false,\"vote_average\":6.9,\"title\":\"Glass\",\"popularity\":338.285,\"poster_path\":\"/gt5KPtwDMeIOPdVfmjYlFw9EetE.jpg\",\"original_language\":\"en\",\"original_title\":\"Glass\",\"genre_ids\":[53,9648,18],\"backdrop_path\":\"/lvjscO8wmpEbIfOEZi92Je8Ktlg.jpg\",\"adult\":false,\"overview\":\"In a series of escalating encounters, security guard David Dunn uses his supernatural abilities to track Kevin Wendell Crumb, a disturbed man who has twenty-four personalities. Meanwhile, the shadowy presence of Elijah Price emerges as an orchestrator who holds secrets critical to both men.\",\"release_date\":\"2019-01-16\"}]}"
    
        let jsonData = jsonString?.data(using: .utf8)!
        
        movieCollection = try! JSONDecoder().decode(MovieCollection.self, from: jsonData!)

        movie = movieCollection?.movies?[0]
    }
    
    override func tearDown() {
        
        jsonString = nil
        movieCollection = nil
        movie = nil
        
    }
    
    func testMovieCollectionAttributes() {
        
        XCTAssertNotNil(movieCollection, "Movies collection object it's nil")
        
        XCTAssertEqual(movieCollection?.page, 1, "Page number doesn't match stored value")
        XCTAssertNotNil(movieCollection?.page, "Attribute page it's nil")
        
        XCTAssertEqual(movieCollection?.totalResults, 19809, "Results count doesn't match stored value")
        XCTAssertNotNil(movieCollection?.totalResults, "Attribute totalResults it's nil")
        
        XCTAssertEqual(movieCollection?.totalPages, 991, "Total pages count doesn't match stored value")
        XCTAssertNotNil(movieCollection?.totalPages, "Attribute totalPages it's nil")
        
        XCTAssertNotNil(movieCollection?.movies, "Movies collection it's nil")
    }
    
    func testMovieAttributes() {
        
        XCTAssertEqual(movie?.voteCount, 935, "Vote count number doesn't match stored value")
        XCTAssertNotNil(movie?.voteCount, "Attribute voteCount it's nil")
        
        XCTAssertEqual(movie?.id, 450465, "Id number doesn't match stored value")
        XCTAssertNotNil(movie?.id, "Attribute id it's nil")
        
        XCTAssertEqual(movie?.video, false, "Attribute video it's true")
        XCTAssertNotNil(movie?.video, "Attribute video it's nil")

        XCTAssertEqual(movie?.voteAverage, 6.9, "Vote average number doesn't match stored value")
        XCTAssertNotNil(movie?.voteAverage, "Attribute voteAverage it's nil")
        
        XCTAssertEqual(movie?.title, "Glass", "Title string doesn't match stored string")
        XCTAssertNotNil(movie?.title, "Attribute title it's nil")
        
        XCTAssertEqual(movie?.popularity, 338.285, "Popularity number doesn't match stored value")
        XCTAssertNotNil(movie?.popularity, "Attribute popularity it's nil")
        
        XCTAssertEqual(movie?.posterPath, "/gt5KPtwDMeIOPdVfmjYlFw9EetE.jpg", "Poster path string doesn't match stored string")
        XCTAssertNotNil(movie?.posterPath, "Attribute posterPath it's nil")
        
        XCTAssertEqual(movie?.originalLanguage, "en", "Original language string doesn't match stored string")
        XCTAssertNotNil(movie?.originalLanguage, "Attribute originalLanguage it's nil")
        
        XCTAssertEqual(movie?.originalTitle, "Glass", "Original title string doesn't match stored string")
        XCTAssertNotNil(movie?.originalTitle, "Attribute originalTitle it's nil")

        XCTAssertEqual(movie?.genreIds, [53, 9648, 18], "Genre ids array doesn't match stored array")
        XCTAssertNotNil(movie?.genreIds, "Attribute genreIds it's nil")
        
        XCTAssertEqual(movie?.backdropPath, "/lvjscO8wmpEbIfOEZi92Je8Ktlg.jpg", "Backdrop path string doesn't match stored string")
        XCTAssertNotNil(movie?.backdropPath, "Attribute backdrop path it's nil")
        
        XCTAssertEqual(movie?.adult, false, "Attribute adult it's true")
        XCTAssertNotNil(movie?.adult, "Attribute adult it's nil")
        
        XCTAssertEqual(movie?.overview, "In a series of escalating encounters, security guard David Dunn uses his supernatural abilities to track Kevin Wendell Crumb, a disturbed man who has twenty-four personalities. Meanwhile, the shadowy presence of Elijah Price emerges as an orchestrator who holds secrets critical to both men.", "Overview string doesn't match stored string")
        XCTAssertNotNil(movie?.overview, "Attribute overview it's nil")
        
        XCTAssertEqual(movie?.releaseDate, "2019-01-16", "Title string doesn't match stored string")
        XCTAssertNotNil(movie?.releaseDate, "Attribute releaseDate it's nil")
    }
}
