//
//  DetailView.swift
//  TheMovieDB
//
//  Created by vortizes on 2/1/19.
//  Copyright © 2019 vortizes. All rights reserved.
//

import UIKit

class DetailView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var votes: UILabel!
    @IBOutlet weak var popularity: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var synopsis: UILabel!
}
