//
//  MovieTableViewCell.swift
//  TheMovieDB
//
//  Created by vortizes on 2/1/19.
//  Copyright © 2019 vortizes. All rights reserved.
//

import UIKit

protocol InfoButtonDelegate {
    func infoButtonTapped(at index:IndexPath)
}

class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    
    var delegate:InfoButtonDelegate!
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @IBAction func infoButtonPressed(_ sender: UIButton) {
        self.delegate?.infoButtonTapped(at: indexPath)
    }
    
}
