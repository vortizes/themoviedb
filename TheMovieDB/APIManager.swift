//
//  APIManager.swift
//  TheMovieDB
//
//  Created by vortizes on 2/1/19.
//  Copyright © 2019 vortizes. All rights reserved.
//

import UIKit

class APIManager: NSObject {
    
    let baseUrl = "https://api.themoviedb.org/3/movie"
    let posterImageEndpoint = "http://image.tmdb.org/t/p/w500"
    let apiKey = "34738023d27013e6d1b995443764da44"
    let apiKeyEndpoint = "?api_key="
    let pageEndpoint = "&page="

    static let sharedInstance = APIManager()
    
    func retrieveMoviesByPage(page: Int,
                              filter: String,
                              onSuccess: @escaping(MovieCollection) -> Void,
                              onFailure: @escaping(Error) -> Void ) {
        
        let pageString = String(page)
        let popularMoviesUrl = baseUrl + filter + apiKeyEndpoint + apiKey + pageEndpoint + pageString
        
        guard let url = URL(string: popularMoviesUrl) else
        { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else { return }
            do {
                
                if error != nil {
                    onFailure(error!)
                } else {
                    let movieCollection = try JSONDecoder().decode(MovieCollection.self, from: data)
                
                    onSuccess(movieCollection)
                }
                
            } catch let jsonErr {
                print("Error parsing json", jsonErr)
            }
        }.resume()
    }
}
