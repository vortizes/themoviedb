//
//  MovieCollection.swift
//  TheMovieDB
//
//  Created by vortizes on 2/1/19.
//  Copyright © 2019 vortizes. All rights reserved.
//

import UIKit

class MovieCollection: Decodable {
    
    let page: Int?
    let totalResults: Int?
    let totalPages: Int?
    let movies: [Movie]?
    
    enum CodingKeys : String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case movies = "results"
    }
}
